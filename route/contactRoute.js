const router = require('express').Router();
const nodemailer = require('nodemailer');

router.post('/contact', (req, res) => {
    let data = req.body;
    if(data.name.length === 0 ||
        data.lastName.length === 0 ||
        data.email.length === 0 ||
        data.phone.length === 0 ||
        data.message.length === 0) {
        return res.json({msg:"Please fill all the fields"})
    }

    let smtpTransport = nodemailer.createTransport({
        service:'Gmail',
        port:465,
        auth:{
            user:'testmailing202@gmail.com',
            pass:'123123123Qqwe'
        }
    })

    let mailOptions = {
        from:'Test Mailing',
        to:'testmailing202@gmail.com',
        subject:`Message from ${data.name}`,
        html:`
        
        <h3>Informations</h3>
        <ul>
        <li>First Name: ${data.name}</li>
        <li>Last Name: ${data.lastName}</li>
        <li>Email: ${data.email}</li>
        <li>Phone: ${data.phone}</li>
       
        </ul>
    
        <h3>Message</h3>
        <p>${data.message}</p>
        `
    }

    smtpTransport.sendMail(mailOptions, (err)=>{

    try {

    if(err) {
        console.log(err);
        return res.status(400).json({msg: 'Something went wrong'})
    }
    res.status(200).json({msg:'Form was sent successfully'})

    } catch (err) {
        if(err) return res.status(500).json({msg:'There is server error'})
    }
    })
})

module.exports = router;
