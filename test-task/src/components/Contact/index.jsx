import React from 'react';
import './styles.scss';
export default function Contact() {
    return <div className='contacts'>
        <div className="content">
            <h4>Contact</h4>
            <h2>Our Location</h2>
            <div className="iframe-container">
                <iframe
                    loading="lazy"
                    title='location'
                    referrerPolicy="no-referrer-when-downgrade"
                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCaXd3yJjWYIJqJGM9mldfC8arnW4V4yMk
    &q=41%C2%B010'40.7%22N%2073%C2%B011'26.7%22W">
                </iframe>
                <div className="iframe-container-info">
                    <h3>Office</h3>
                    <address>9018 S 550th E <br/>
                        Carbon, Indiana(IN), 47837
                    </address>
                    <div className="links">
                        <a href="tel:+(203)456-7890">Phone: (203) 456-7890</a>
                        <a href="tel:+(203)456-7890">Fax: (203) 654-0987</a>
                        <a href="mailto:info@lawyersgroup.com">info@lawyersgroup.com</a>
                    </div>
                    <button>
                        Contact us
                    </button>
                </div>
            </div>
        </div>
    </div>
}
