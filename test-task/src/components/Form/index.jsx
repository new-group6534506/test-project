import React, { useState } from 'react'
import axios from 'axios';
import {FiLoader} from 'react-icons/fi';
import './styles.scss';

export default function Form() {
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const formSubmit = async (e) => {
        e.preventDefault();
        try {
            let data = {
                name,
                lastName,
                email,
                phone,
                message,
            }
            setLoad(true);
            const res = await axios.post(`/contact`, data).catch(()=>{
                setErrorMessage(res.data.msg);
                setTimeout(()=> setErrorMessage(''), 2000);
            })

            if(res.status === 200){
                setSuccessMessage(res.data.msg);
                setTimeout(()=> setSuccessMessage(''), 2000);

                setName('');
                setLastName('');
                setPhone('');
                setEmail('');
                setMessage('');
                setLoad(false);
            }
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <div className="feedback">
            <div className="feedback-content">
                <h4>Consultation form</h4>
                <h2>Get a free Case Evaluation</h2>
                <p>Lawyers Group attorneys are always ready
                    to review your case at absolutely no cost to you.
                    We’ll answer questions that you may have,
                    and help you get back on your feet.
                </p>
                <div className="back-form">
                    {successMessage && <div className='res-message-success'>{successMessage}</div>}
                    {errorMessage && <div className='res-message-error'>{errorMessage}</div>}
                    <form onSubmit={formSubmit} >
                        <div className="inputs">
                            <div className="input-container">
                                <input type="text"
                                       name='first-name'
                                       onChange={e => setName(e.target.value)}
                                       value={name}
                                       required={true}
                                />
                                <label className="placeholder-text" htmlFor="first-name" id="placeholder-first-name">
                                    <div className="text">First Name</div>
                                </label>
                            </div>
                            <div className="input-container">
                                <input type="text"
                                       name='last-name'
                                       onChange={e => setLastName(e.target.value)}
                                       value={lastName}
                                       required={true}
                                />
                                <label className="placeholder-text" htmlFor="last-name" id="placeholder-last-name">
                                    <div className="text">Last Name</div>
                                </label>
                            </div>
                            <div className="input-container">
                                <input type="email"
                                       name='email'
                                       onChange={e => setEmail(e.target.value)}
                                       value={email}
                                       required={true}
                                />
                                <label className="placeholder-text" htmlFor="email" id="placeholder-email">
                                    <div className="text">Your Email</div>
                                </label>
                            </div>
                            <div className="input-container">
                                <input type="text"
                                       name='phone'
                                       onChange={e => setPhone(e.target.value)}
                                       value={phone}
                                       required={true}
                                />
                                <label className="placeholder-text" htmlFor="phone" id="placeholder-phone">
                                    <div className="text">Phone number</div>
                                </label>
                            </div>

                        </div>
                        <textarea
                            onChange={e => setMessage(e.target.value)}
                            value={message}
                            placeholder="Tell us about your"
                            name="message"
                            required={true}
                        />
                        <input type="submit" value='Schedule your free consultation'/>
                    </form>
                </div>
            </div>
            {load && <div className='loading-container'><FiLoader size={32}/></div>}
        </div>
    )
}

