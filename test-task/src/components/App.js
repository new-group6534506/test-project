import Form from "./Form";
import Contact from "./Contact";
import Footer from "./Footer";
import './App.css';

function App() {
  return (
    <div className="App">
      <Form/>
      <Contact/>
      <Footer/>
    </div>
  );
}

export default App;
