import React from 'react';
import {
    AiOutlineTwitter,
    AiFillLinkedin,
    AiFillFacebook,
} from 'react-icons/ai';

import './styles.scss';
export default function Footer() {
    return <footer>
        <div className="content">
            <div className='brand'>
                <span>LAYERS</span>
                <span>GROUP.LLC</span>
            </div>
            <div className="footer-contacts">
                <h4>Contacts</h4>
                <div className="links">
                    <a href="tel:+(203)456-7890">Phone: (203) 456-7890</a>
                    <a href="tel:+(203)456-7890">Fax: (203) 654-0987</a>
                    <a href="mailto:info@lawyersgroup.com">info@lawyersgroup.com</a>
                </div>
            </div>
            <div className="office">
                <h4>Office</h4>
                <address>
                    <span>9018 S 550th E</span>
                    <span>Carbon, Indiana(IN), 47837</span>
                </address>
            </div>
            <div className="social">
                <h4>Social</h4>
                <div className="social-networks">
                    <a href="https://ua.linkedin.com/">
                        <AiFillLinkedin size={20} color='#fff'/>
                    </a>
                    <a href="https://twitter.com/">
                        <AiOutlineTwitter size={20}/>
                    </a>
                    <a href="https://www.facebook.com/">
                        <AiFillFacebook size={20}/>
                    </a>
                </div>
            </div>
        </div>
        <hr/>
        <div className="content">
            <div className='rights'>
                <span>© {new Date().getFullYear()} Lawyers Group  |  All Rights Reserved</span>
                <span className="privacy">
                    <a href="/">Privacy Policy</a>
                </span>
            </div>

        </div>
    </footer>
}
